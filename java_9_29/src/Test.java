import java.util.Scanner;

public class Test {
   /* public static int max2(int x, int y){
       return Math.max(x, y);
    }
    public static int max3(int x, int y,int z){
        int a = max2(x,y);
        return max2(a,z);
    }

    public static void main(String[] args) {
        int a=55,b=22,c=33;
        int max = max3(a,b,c);
        System.out.println(max);
    }//创建方法求两个数的最大值max2，随后再写一个求3个数的最大值的函数max3。
//要求：在max3这个函数中，调用max2函数，来实现3个数的最大值计算*/

    /*public static int factorial(int n){

        if(n>1) {
           return n * factorial(n - 1);
        }else{
            return 1;
        }
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int n = factorial(num);
        System.out.println(n);
    }//求n的阶乘*/

    public static int factorial(int n){

        if(n>1) {
            return n * factorial(n - 1);
        }else{
            return 1;
        }
    }


    public static void main(String[] args) {

            Scanner scanner = new Scanner(System.in);
            int num = scanner.nextInt();
            int sum = 0;
        for (; num >= 1; num--) {
            int n = factorial(num);
            sum += n;
        }
        System.out.println(sum);
    }
}//求1！+2！+3！+4！+........+n!的和
