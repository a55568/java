import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import javax.xml.bind.Element;
import java.util.List;

public class FirstTest {

    WebDriver driver = null;
    void createDriver() throws InterruptedException {
        // 直接指定本地的chrome驱动位置
        System.setProperty("webdriver.chrome.driver","D:\\code\\java\\java\\java_ceshi\\chromedriver-win64\\chromedriver.exe");
        //1.打开浏览器  使用驱动来打开
        // 这一行代码表示是从网上去下载chrome驱动
//        WebDriverManager.chromedriver().setup();

        //增加浏览器配置：创建驱动对象要强制指定允许访问所有链接
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");

        driver = new ChromeDriver(options);

        //2.输入完整的网址：https://www.baidu.com
        driver.get("https://www.baidu.com");
        Thread.sleep(3000);
    }


    //测试百度搜索关键词：迪丽热巴
    void test01() throws InterruptedException {

        createDriver();


        //3.找到输入框，并输入关键词：迪丽热巴
        driver.findElement(By.cssSelector("#kw")).sendKeys("迪丽热巴");
        Thread.sleep(3000);

        //4.找到百度一下按钮，并点击
        driver.findElement(By.cssSelector("#su")).click();
        Thread.sleep(3000);

        //5.关闭浏览器
        driver.quit();
    }

    //元素的定位
    void test02() throws InterruptedException {
        createDriver();
        //选择器
        //driver.findElement(By.cssSelector("#s-hotsearch-wrapper > div > a.hot-title > div"));
        //活得单个元素

        //List<WebElement> ll = driver.findElements(By.cssSelector("#hotsearch-content-wrapper > li > a > span.title-content-title"));
        //获得多个元素

        //这里要保持页面状态一致（比如是否同为登录状态）

//        for (int i = 0; i < ll.size(); i++) {
//            System.out.println(ll.get(i).getText());
//        }
//
//        driver.quit();


//        String title = driver.getTitle();
//        String url = driver.getCurrentUrl();
//
//        System.out.println("titlr =" + title);//获取标题
//        System.out.println("Url =" + url);//获取url

        //设置窗口大小

        driver.manage().window().minimize();//最小化
        Thread.sleep(3000);

        driver.manage().window().maximize();
        Thread.sleep(3000);

        driver.manage().window().fullscreen();//全屏
        Thread.sleep(3000);

        driver.manage().window().setSize(new Dimension(1600,724));
        Thread.sleep(3000);

        driver.quit();
    }


}
