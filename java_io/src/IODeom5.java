import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class IODeom5 {
    public static void main(String[] args) throws IOException {
        try(InputStream inputStream = new FileInputStream("./text2.txt")){
            while (true) {
                byte[] buffer = new byte[1024];
                int n = inputStream.read(buffer);
                if (n == -1) {
                    //文件读取完毕
                    break;
                }

                String s = new String(buffer,0,n);//打印汉字
                System.out.println(s);
            }
        }
    }
}
