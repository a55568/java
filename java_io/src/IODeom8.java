import java.io.*;
import java.util.Scanner;

public class IODeom8 {
    public static void main(String[] args) {
        //1.输入必要信息
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入要被复制的源文件：");
        String SrcPath = scanner.next();
        System.out.println("输入要复制的目标文件：");
        String destPath = scanner.next();

        //判定合法性
        //SrcPath对应的文件是否存在
        File srcFile = new File(SrcPath);
        if(!srcFile.isFile()){
            System.out.println("源文件目录有问题");
            return;
        }

        //destPath不要求对应的为你教案存在 但是目标目录得存在
        File destFile = new File(destPath);
        if(!destFile.getParentFile().isDirectory()){
            System.out.println("目标路径有问题");
            return;
        }

        //复制操作\
        //相当于边读边写
        try(InputStream inputStream = new FileInputStream(SrcPath);
            OutputStream outputStream = new FileOutputStream(destFile)) {

            while (true){
                byte[] buffer = new byte[1024];
                int n = inputStream.read();
                if(n == -1){
                    //读取完毕
                    break;
                }

                //把读取到的内容写到
                outputStream.write(buffer,0,n);
            }


        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
