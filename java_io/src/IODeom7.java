import java.io.File;
import java.util.Scanner;

public class IODeom7 {
    public static void main(String[] args) {
        //1.输入必要信息
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入要搜索的文件名：");
        String FileName = scanner.next();
        System.out.println("输入要被搜索的目录：");
        String rootPath = scanner.next();
        File rootFile = new File(rootPath);
        if(!rootFile.isDirectory()){
            System.out.println("输入的路径有问题");
            return;
        }

        //2,有了要搜索的路径。按照递归的方式
        sanDir(rootFile,FileName);
    }

    private static void sanDir(File rootFile, String fileName) {
        //1 把当前目录中的文件和子目录都列出来
        File[] files = rootFile.listFiles();
        if(files == null){
            //空的目录 返回即可
            return;
        }

        //2 遍历上诉files，判断是目录还是文件
        for (File f:files) {
            if(f.isFile()){
                //普通文件，判断是不是要搜索的文件
                if(f.getName().equals(fileName)){
                    System.out.println("找到了要搜索的文件"+ f.getAbsoluteFile());
                }
            }else {
                //目录文件需要进一步递归
                sanDir(f,fileName);
            }
        }
    }
}
