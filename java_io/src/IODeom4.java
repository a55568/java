import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class IODeom4 {
    public static void main(String[] args) throws IOException {

        try(InputStream inputStream = new FileInputStream("./text2.txt")){//打开文件，用try就不用手关闭文件了
            //读文件
            //不知道有多少字节，使用while来完成
           /* while (true){
                int b = inputStream.read();//一次读一个字节
                if(b == -1){
                    //文件读取完了
                    break;
                }
                //打印这个字节的数据
                System.out.printf("%x ",b);//
            }*/


            //也可以一次读若干个字节

            //读文件
           while (true){
                byte[] buffer = new byte[1024];//相当于一次读1024个字节(可以改变)--相当于读出来的字节填满这个缓冲区 也就是一次填充1024个（自己定的）
                int n = inputStream.read(buffer);
                if(n == -1 ){
                    //文件读取完毕
                    break;
                }
                for (int i = 0; i < n; i++) {
                    System.out.printf("%x ",buffer[i]);//打印缓冲区
                }
           }
        }
    }
}
