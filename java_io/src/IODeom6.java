import java.io.*;

public class IODeom6 {
    public static void main(String[] args) throws IOException {
        try(OutputStream outputStream = new FileOutputStream("./text2.txt",true)){//append可以达到追加型的效果
            //写文件（整个数组），新写的东西会覆盖原来文件的内容
            byte[] buffer = new byte[]{96,97,98,99,95,94};
            outputStream.write(buffer);

        }
    }

}
