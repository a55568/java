public class MyQueue {
    static class ListNode{
        private int val;
        private ListNode prev;
        private ListNode last;

        public ListNode(int val){
            this.val = val;
        }
    }

    private ListNode front;//队头
    private ListNode rear;//队尾

    private int usedSize;


    //头插法 进队列
    public void offer(int x){
        ListNode node = new ListNode(x);
        if(front == null){
            front = rear = node;
        }else {
            node.last = front;
            front.prev = node;
            front = node;
        }
        this.usedSize++;
    }

   //出队列 删除最后一个节点
    public int  poll(){
        if(front == null){
            return -1;
        }
        int ret = rear.val;
        if(front == rear){
            usedSize--;
            return  -1;

        }
        rear.prev.last = null;
        rear = rear.prev;
        usedSize--;
        return ret;
    }

    public int peek(){
        if(front == null){
            return -1;
        }
        return rear.val;
    }
}
