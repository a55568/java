import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int [] array = {5,6,2,3,9,8,5,7,7,7,8,8,2,2,3,3};

        Sort.bubbleSort(array);
        System.out.println(Arrays.toString(array));

        Sort.quickSort(array);
        System.out.println(Arrays.toString(array));

        Sort.mergeSort(array);
        System.out.println(Arrays.toString(array));
      }
}
