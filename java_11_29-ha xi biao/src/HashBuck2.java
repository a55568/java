import java.util.Arrays;
import java.util.Objects;



class Person{
    public String iD;

    public Person(String iD) {
        this.iD = iD;
    }
}

public class HashBuck2<K,V> {
    public class Node<K,V>{
        public K key;
        public V val;
        public Node<K,V> next;

        public Node(K key, V val) {
            this.key = key;
            this.val = val;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HashBuck2<?, ?> hashBuck2 = (HashBuck2<?, ?>) o;
        return usedSize == hashBuck2.usedSize && Arrays.equals(array, hashBuck2.array);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(usedSize);
        result = 31 * result + Arrays.hashCode(array);
        return result;
    }

    public Node<K,V>[] array;
    public int usedSize;

    public HashBuck2(){
        array = (Node<K,V>[]) new Node[10];
    }

    public void put(K key, V val){
        int hash = key.hashCode();//将对象转化为整数
        int index = hash % array.length;
        Node<K,V> node = new Node<>(key,val);

        Node<K,V> cur = array[index];

        //此时没有节点
        if(cur == null){
            array[index] = node;
            usedSize++;
            return;
        }

        //找到尾巴
        while (cur.next != null){
            //更新val值
            if(cur.key.equals(key)){
                cur.val = val;
                return;
            }
            cur = cur.next;
        }

        if(cur.key.equals(key)){
            cur.val = val;
            return;
        }
        cur.next = node;//进行插入
        usedSize++;

    }

    public V get(K key){
        int hash = key.hashCode();
        int index = hash % array.length;
        Node<K,V> cur = array[index];
        while (cur != null){
            if(cur.key.equals(key)){
                return cur.val;
            }
            cur = cur.next;
        }
        return null;
    }
}
