import java.util.*;

public class Test {
    public static List<List<Integer>> generate(int numRows){

        List<List<Integer>> ret = new ArrayList<>();//类比二维数组

        List<Integer> row = new ArrayList<>();
        row.add(1);
        ret.add(row);

        for (int i = 1; i < numRows; i++) {
            List<Integer> row1 = new ArrayList<>();//新的一行
            row1.add(1);//新的一行第一个数

            //处理中间列的数
            List<Integer> lastRow = ret.get(i-1);//获取上一行
            for (int j = 1; j < i; j++) {

                int index = lastRow.get(j) + lastRow.get(j-1);
                row1.add(index);
            }

            row1.add(1);//新的一行最后一个数

            ret.add(row1);
        }

        for(int i = 0;i < ret.size(); i++){//ret里面存了size个一维数组
            for(int j = 0;j < ret.get(i).size();j ++){//每个一维数组里存了每一行的数列
                System.out.print(ret.get(i).get(j) + " ");
            }
            System.out.println();
        }

        return ret;

    }

    public static void main(String[] args) {

        generate(8);
    }
}
