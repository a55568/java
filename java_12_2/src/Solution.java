import java.util.HashMap;


//随复制的链表
class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}

   //可以画图（数据结构倒数第三节）
    public class Solution {
        public Node copyRandomList(Node head) {
            HashMap<Node, Node> map = new HashMap<>();//利用hasmap将新老节点建立联系
            Node cur = head;

            while (cur != null) {
                Node node = new Node(cur.val);
                map.put(cur, node);
                cur = cur.next;
            }

            cur = head;
            //完成题意
            while (cur != null) {
                map.get(cur).next = map.get(cur.next);
                map.get(cur).random = map.get(cur.random);
                cur = cur.next;
            }

            return map.get(head);
        }
    }



