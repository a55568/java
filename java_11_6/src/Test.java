public class Test {

  //相交链表
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        int lenA = number(headA);
        int lenB = number(headB);
        ListNode pL = headA;
        ListNode pS = headB;
        int k = lenA - lenB;
        if(k < 0){
            pL = headB;
            pS = headA;
            k = lenB - lenA;
        }//保证pL永远指向长的链表，k永远是正值

        for(int i = 0;i < k;i++){
            pL = pL.next;
        }

        while(pL != pS && pL != null){
            if(pS == null){
                return null;
            }
            pL = pL.next;
            pS = pS.next;
        }

        return pS;
    }

    public int number(ListNode head){//求链表的长度
        int count = 0;
        ListNode cur = head;
        while(cur != null){
            count++;
            cur = cur.next;
        }
        return count;
    }

}
