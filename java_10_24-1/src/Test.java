
public class Test {


    public static String ToLowerCase(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }

        if (s == null || s.length() == 0) {
            return s;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            char ch = s.charAt(i);
            if (ch >= 65 && ch <= 90) {
                ch |= 32;
            }
            sb.append(ch);//将每一个得到的s中的字符传给sb这个对象
        }
        return sb.toString();
    }
    public static void main(String[] args) {
        String s = "Abcd";
        String ch = ToLowerCase(s);
        System.out.println(ch);
    }


    //模拟实现库函数toLowerCase

    //大写变小写、小写变大写：字符 ^= 32;
    //大写变小写、小写变小写：字符 |= 32;
    //大写变大写、小写变大写：字符 &= 33;

}
