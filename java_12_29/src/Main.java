
// Main.java
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TicTacToeAI game = new TicTacToeAI();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            game.printBoard();

            if (game.currentPlayer == 'X') {
                System.out.println("Player's move (enter row and column, e.g., 0 0): ");
                int row = scanner.nextInt();
                int col = scanner.nextInt();

                if (game.isValidMove(row, col)) {
                    game.makeMove(row, col);
                } else {
                    System.out.println("Invalid move! Try again.");
                    continue; // Skip to the next iteration of the loop
                }
            } else {
                int[] bestMove = game.getBestMove();
                game.makeMove(bestMove[0], bestMove[1]);
            }

            char winner = game.checkWinner();
            if (winner != ' ') {
                game.printBoard();
                System.out.println("Winner: " + winner);
                break;
            }

            if (game.isDraw()) {
                game.printBoard();
                System.out.println("It's a draw!");
                break;
            }
        }

        scanner.close();
    }
}
