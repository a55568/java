
// TicTacToe.java
public class TicTacToe {
    private char[][] board = new char[3][3];
    public char currentPlayer = 'X'; // 'X' for Player, 'O' for AI

    // Initialize the board
    public TicTacToe() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = ' ';
            }
        }
    }

    // Print the board
    public void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    // Check if a move is valid
    public boolean isValidMove(int row, int col) {
        return board[row][col] == ' ';
    }

    // Make a move
    public void makeMove(int row, int col) {
        board[row][col] = currentPlayer;
        switchPlayer();
    }

    // Switch the current player
    private void switchPlayer() {
        currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
    }

    // Check if there is a winner
    public char checkWinner() {
        // Check rows, columns, and diagonals
        // ... (omitted for brevity)
        return ' '; // No winner
    }

    // Check if the game is a draw
    public boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }
}


