package fu_xie_0;

public class Solution {
    public void duplicateZeros(int[] arr) {
        int cur = 0;
        int dest = -1;
        //先找到最后一个复写的数
        while(cur < arr.length){
            if(arr[cur] != 0){
                dest ++;
            }else{
                dest += 2;
            }

            if(dest >= arr.length -1){//此时已经找到
                break;
            }
            cur++;
        }

        //处理边界情况(这个是从正着找cur推出来的)
        if(dest == arr.length){
            arr[arr.length - 1] = 0;
            cur--;
            dest -= 2;
        }

        //从后向前完成复写
        while(cur >= 0){
            if(arr[cur] != 0){
                arr[dest] = arr[cur];
                cur --;
                dest --;
            }else{
                arr[dest] = 0;
                arr[dest - 1] = 0;
                cur --;
                dest -= 2;
            }
        }
    }
}
