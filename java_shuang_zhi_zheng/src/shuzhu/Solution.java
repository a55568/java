package shuzhu;

public class Solution {
    public void moveZeroes(int[] nums) {
        int dest = -1;
        int cur = 0;
        while (cur != nums.length) {
            if (nums[cur] != 0) {
                dest++;
                swap(dest,cur,nums);
            }

            cur++;
        }
    }

    public void swap(int x, int y,int[] nums) {

        int tmp = nums[x];
        nums[x] = nums[y];
        nums[y] = tmp;
    }

    //1.左指针左边均为非零数；
    //2.右指针左边直到左指针处均为零
    //3.右指针右边都是未处理的数据
    //注意数组进分块处理时，可以用到双指针的思想！！
}
