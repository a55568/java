package thread;

import java.util.Timer;
import java.util.TimerTask;

public class MyThreadDemo14  {
    public static void main(String[] args) throws InterruptedException {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {//默认是前台线程
            @Override
            public void run() {//时间到了要执行的代码
                System.out.println("hello timer3000");
            }
        },3000);//等待时间

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello timer2000");
            }
        },2000);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello timer1000");
            }
        },1000);

        System.out.println("hello main");
        Thread.sleep(4000);
        timer.cancel();//主动让timer结束
    }
}
