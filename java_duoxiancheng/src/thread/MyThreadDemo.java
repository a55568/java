package thread;

//1.创建一个自己的类，继承这个 Threed
class MyThread extends Thread{
    public void run(){
        //run方法就是该线程的入口方法
        System.out.println("hello world");
    }
}
public class MyThreadDemo {
    public static void main(String[] args) {
        //2.根据刚才的类，创建出实例（线程实例，才是真的正的线程）
        Thread thread = new MyThread();
        //3.调用thread的start的方法，调用系统的api，在系统封中创建出线程
        thread.start();
    }
}
