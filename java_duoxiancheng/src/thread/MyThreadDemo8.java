package thread;

public class MyThreadDemo8 {
    public static void main(String[] args) {

        Thread t = new Thread(() ->{
            while (!Thread.currentThread().isInterrupted()) {//作用是获取当前线程类似与this
                System.out.println("我是一个线程，正在工作中");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
            //当前时死循环给了个错误提示
            System.out.println("线程工作完成！");
        });

        t.start();

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("让t线程结束！");
        t.interrupt();//sleep提前唤醒了


    }
}
