package thread;

import static java.lang.Thread.*;

class MyThread3 extends Thread{
    @Override
    public void run() {
        while (true) {
            System.out.println("hello world");
            //它只能用try catch 是因为 父类 thread 没有throws，所以子类重写ru方法时不能用抛出异常！！！
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
public class MyThread2 {
    public static void main(String[] args) {
        Thread myThread3 = new MyThread3();
        myThread3.start();


        while (true) {
            System.out.println("hello main");
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
