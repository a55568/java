package thread;

public class MyThreadDemo4 {
    public static void main(String[] args) {
        Thread t = new Thread( ()-> {   //lambda 表达式 和重写runnable 的run方法类似  最主流的写法（匿名函数）

                while (true) {
                    System.out.println("hello Thread");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

        });


        t.start();

        while (true){
            System.out.println("hello main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
