package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyThreadDemo15 {//创建线程池对象
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(4);//线程数目固定（4个）
        service.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello");
            }
        });
    }
}
