package thread;

public class MyThreadDemo11 {
    public static void main(String[] args) {
        Object locker = new Object();

        Thread t1 = new Thread(() ->{
           synchronized (locker){
               System.out.println("t1在wait之前 ");
               try {
                   locker.wait();  //阻塞等待（释放锁进行阻塞）
               } catch (InterruptedException e) {
                   throw new RuntimeException(e);
               }
               System.out.println("t2在wait之后");
           }
        });

        Thread t2 = new Thread(() ->{

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            synchronized (locker){
                System.out.println("t2在notify之前");
                locker.notify();  //唤醒被阻塞的t1（被唤醒后t1就又可以去就竞争锁了）
                System.out.println("t2在notify之后");
            }
        });

        t1.start();
        t2.start();
    }
}
