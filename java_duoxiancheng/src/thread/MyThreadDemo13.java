package thread;

class  SingletonLazy{
    //懒汉的方式实现单个实例
    private static volatile SingletonLazy instance = null;// 1，volatile 是保证执行顺序不变（都是为了线程安全）如果另外一个线程判断instance
                                                          //是不为null但是还没初始化，继续使用instance的属性和方法（全0值）就会出现问题
               //被volatile修饰的变量的读写操作的顺序是不能被重排序的--》防止发生1的情况

    private static Object locker = new Object();

    public static SingletonLazy getInstance() {
        if(instance == null) {//2，双重判断--节省加锁解锁的时间，提高效率
            synchronized (locker) {//3，不能随意加锁
                if (instance == null) {
                    instance = new SingletonLazy();
                }
            }
        }

        return instance;

    }

    private SingletonLazy(){

    }
}

public class MyThreadDemo13 {
    public static void main(String[] args) {
        SingletonLazy s1 = SingletonLazy.getInstance();
        SingletonLazy s2 = SingletonLazy.getInstance();
        System.out.println(s1 == s2);
    }
}
