package thread;

public class MyThreadDemo10 {
    public static void main(String[] args) {

        Object o1 = new Object();
        Object o2 = new Object();

        Thread t1 = new Thread(() ->{
            synchronized (o1){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                synchronized (o2){
                    System.out.println("t1拿到了俩把锁");
                }

            }
        });

        Thread t2 = new Thread(() ->{
            synchronized (o1) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                synchronized (o2){
                    System.out.println("t2拿到了俩把锁");
                }
            }
        });

        t1.start();
        t2.start();
    }
}
