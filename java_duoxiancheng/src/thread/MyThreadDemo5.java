package thread;

public class MyThreadDemo5 {
    public static void main(String[] args) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    System.out.println("hello thread");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        },"这是我的线程");//可以给线程起名字（最好是有描述性 ）


        t.setDaemon(true);//将线程设置成后台线程（不会阻止进程结束）  必须写到t.start()的前面

        //我们创建的线程默认是前台线程，会阻止进程结束，只要线程没结束，进程就不会结束
        t.start();
    }
}
