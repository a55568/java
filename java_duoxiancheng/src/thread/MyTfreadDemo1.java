package thread;

class MyThread4 implements Runnable{
    public void run(){
        while (true) {
            System.out.println("hello Thread");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class MyTfreadDemo1 {
    public static void main(String[] args) {

        Runnable runnable = new MyThread4();//类似于动态绑定
        Thread t = new Thread(runnable);

        t.start();

        while (true) {
            System.out.println("hello main");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
