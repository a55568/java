package thread;


class Singleton{
    //就期望这个类有单个实例
    private static Singleton instance = new Singleton();//私有实例只能调用方法来获取，不能通过实例化
    //类被加载时，这个初始化这个静态成员


    public static Singleton getInstance() {
        return instance;
    }

    private Singleton(){

    }
}

public class MyThreadDemo12 {

    public static void main(String[] args) {
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        System.out.println(s1 == s2);
    }
}
