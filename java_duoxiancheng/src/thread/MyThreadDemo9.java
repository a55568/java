package thread;

public class MyThreadDemo9 {
    /*public static int count1 = 0;*/
    public static int count2 = 0;

    public static void main(String[] args) throws InterruptedException {

        //随便创建一个对象
        Object locker = new Object();

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronized (locker) {//进行加锁
                    count2++;
                }
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronized (locker) {//针对通体个对象进行枷锁 就会竞争阻塞！！
                    count2++;
                }
            }
        });

        t1.start();
        t2.start();

       t1.join();
       t2.join();

        System.out.println(count2);
    }
}
