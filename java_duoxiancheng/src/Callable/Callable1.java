package Callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Callable1 {//原子类的一中，不用上锁

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Callable<Integer> callable = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int result = 0;
                for (int i = 1; i <= 1000; i++) {
                    result += i;
                }
                return result;
            }
        };

        FutureTask<Integer> futureTask = new FutureTask<>(callable);//相当于粘合剂，将线程和callable原子类结合在一起

        Thread t = new Thread(futureTask);
        t.start();

        //接下来代码也不用jion，因为futureTask.get()有阻塞功能 会等到futureTask获取到结果
        System.out.println(futureTask.get());
    }
}
