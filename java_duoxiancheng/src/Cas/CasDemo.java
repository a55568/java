package Cas;

import java.util.concurrent.atomic.AtomicInteger;

public class CasDemo {

    //用到cas的方式实现

    //不适用原生的int，而替换成AtomicInteger(原子类对象)--保证线程安全（没有加锁操作）
    private static AtomicInteger count = new AtomicInteger();
    //public static int count = 0;

    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread( ()->{
            for (int i = 0; i < 5000; i++) {
                count.getAndIncrement();
            }
        });

        Thread t2 = new Thread( ()->{
            for (int i = 0; i < 5000; i++) {
                count.getAndIncrement();
            }
        });

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println(count.get());
    }


}
