import java.util.Locale;
import java.util.Scanner;

public class Test {
    private static boolean isLegal(char ch){
        if ((ch >= 'a' && ch <= 'z'|| ch >= '0'&& ch <= '9')){
            return true;
        }
        return false;
    }//写一个方法判断字符是否合法
    public static boolean isPalindrome(String s){
        s = s.toLowerCase();
        int right = s.length() - 1;
        int left = 0;
        while (left < right) {
            while (left < right && !isLegal(s.charAt(left))) {
                left ++;
            }//不合法就向后走，直到合法

            while (left < right && !isLegal(s.charAt(right))) {
                right --;
            }//不合法就前走，直到合法

            //此时已经合法，对字符进行判断
            if(s.charAt(left) == s.charAt(right)){
                right --;
                left ++;
            }else
                return false;//相同就一起移动，不同就直接返回false

        }

        return true;//循环结束证明是回文串，返回true
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        boolean palindrome = isPalindrome(s);
        System.out.println(palindrome);
    }
}
