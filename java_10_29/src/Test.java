import java.util.Scanner;
 class PasswordError extends Exception{
    public PasswordError(String message) {
        super(message);
    }
}
//用户名错误异常类
class UserError extends Exception{
    public UserError(String message){
        super(message);
    }
}

public class Test {
    private static String userName = "12345678";
    private static String passerWord = "123456";

    public static void main(String[] args) throws UserError, PasswordError {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入账号：");
        String str1 = sc.nextLine();
        System.out.print("请输入密码：");
        String str2 = sc.nextLine();
        login(str1, str2);
    }

    private static void login(String userName, String passerWord) throws PasswordError, UserError {
        if (!Test.userName.equals(userName)){
            //判断用户名是否输入错误
            throw new UserError("用户名错误");//抛出用户名错误异常
        }
        if (!Test.passerWord.equals(passerWord)){
            //判断密码是否输入错误
            throw new PasswordError("密码错误");//抛出密码错误异常
        }
        System.out.println("登录成功");
    }
}

