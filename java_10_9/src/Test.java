import java.util.Arrays;

public class Test {
    public static void main1(String[] args) {
        int[] arr = new int[]{1,5,3,2,8,5,6};
        int flag = 1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length-i-1; j++) {
                if(arr[j+1] < arr[j]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                    flag = -1;
                }
            }
            if(flag == 1) {//如果某次循环 数组已经满足要求 直接跳出循环
                break;
            }
        }
        System.out.print(Arrays.toString(arr));


    }//冒泡排序

    public static void main(String[] args) {
        int[] arr = new int[]{1,2,34,3,4,5,7};
        int flag = 1;
        for (int i = 0; i < arr.length-3; i++) {
            if(arr[i] % 2 !=0 && arr[i+1] % 2 !=0 && arr[i+2] % 2 !=0){
                System.out.println(true);
                flag = -1;
            }
        }
        if(flag == 1){
            System.out.println(false);
        }
    }
}
