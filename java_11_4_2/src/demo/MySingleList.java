package demo;

import java.util.List;

public class MySingleList {
    static class ListNode{
        public int val;//节点的值域
        public ListNode next;//下一个节点的地址

        public ListNode(int val){
            this.val = val;
        }



    }
    public ListNode head;//表示当前链表的头节点

    public void createList() {
        ListNode node1 = new ListNode(2);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(2);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;

        this.head = node1;
    }

    public void display(){
        ListNode cur = head;//防止头指针丢失
        while (cur != null){
            System.out.println(cur.val+" ");
            cur = cur.next;
        }
    }

    //得到单链表长度
    public  int size(){
        int count = 0;
        ListNode cur = head;
        while (cur != null){
            cur = cur.next;
            count++;
        }
        return count;
    }

    //查找是否包含关键字key是否在链表中
    public boolean contains(int key){
        ListNode cur = head;
        while (cur != null) {
            if(cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;


    }
    //任意位置插入
    public void addIndex(int index,int data){
        ListNode cur = head;
        ListNode node9 = new ListNode(data);
        for (int i = 0; i < index-1; i++) {
            cur = cur.next;
        }
        node9.next = cur.next;
        cur.next = node9;
    }


    //删除全部关键字key在链表中
    public void removeAllKey(int key){
        ListNode cur = head.next;
        ListNode prev = head;

        if(head.val == key){
            head = head.next;
        }


        while (cur != null){
            if(cur.val == key){
                prev.next = cur.next;
                cur = cur.next;
            }else {
                prev = cur;
                cur = cur.next;
            }
        }

    }

    //逆置链表
    public ListNode reverseList() {
        if(head == null){
            return null;
        }
        if(head.next == null){
            return head;
        }
        ListNode cur = head.next;
        head.next = null;

        while(cur != null){
            ListNode curNext = cur.next;
            cur.next = head;
            head = cur;
            cur = curNext;
        }
        return head;
    }

    //返回链表中间节点

    public ListNode middleNode() {
        if(head.next == null){
            return head;
        }
        ListNode fast = head.next.next;//快慢指针
        ListNode slow = head.next;

        while(fast != null && fast.next != null){
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }


    //获取链表倒数第k个节点

    public ListNode node(int k){
        int count = 0;
        ListNode fast = head;//快慢指针，使fast和slow中间间隔k，当fast走到尾节点，slow就是目标节点
        ListNode slow = head;
        while (count < k - 1) {
            fast = fast.next;
            count ++;
            if(fast == null){
                return null;
            }
        }

        while (fast.next != null ){
            fast = fast.next;
            slow = slow.next;
        }

        return slow;
    }


    //合并俩个有序链表

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {

        ListNode newHead = new ListNode(1);//不具备有效数据，创建一个新的节点
        ListNode tH = newHead;

        while(list1 != null && list2 != null){

            if(list1.val > list2.val){   //画图理解这个过程
                tH.next = list2;
                tH = tH.next;
                list2 = list2.next;
            }else{
                tH.next = list1;
                tH = tH.next;
                list1 = list1.next;
            }

        }

        if(list1 != null){
            tH.next = list1;
        }
        if(list2 != null){
            tH.next = list2;
        }

        return newHead.next;
    }

    //头插法

    public void addFirst(int val){
        ListNode node = new ListNode(val);
        node.next =head;
        head = node;
    }

    //给定 x, 把一个链表整理成前半部分小于 x, 后半部分大于等于 x 的形式
    public ListNode partition(ListNode pHead, int x) {
        ListNode bs = null;
        ListNode be = null;
        ListNode as = null;
        ListNode ae = null;

        ListNode cur = pHead;
        //遍历链表
        while (cur != null) {//遍历链表
            if (cur.val < x) {
                if (bs == null) {//第一次
                    bs = be = cur;
                } else {
                    be.next = cur;
                    be = be.next;
                }
            } else {
                if (as == null) {
                    as = ae = cur;
                } else {
                    ae.next = cur;
                    ae = ae.next;
                }

            }
            cur = cur.next;

        }
        if(bs == null){//防止第一段没有数据
            return as;
        }
        be.next = as;
        if(as != null){//将链表的末尾的next置为空，防止空指针
            ae.next = null;
        }
        return bs;


    }


    //判断链表是否回文
    public boolean chkPalindrome(ListNode head) {

        //找到中间节点
        ListNode fast = head;
        ListNode slow = head;
        while(fast !=null && fast.next !=null){
            fast = fast.next.next;
            slow = slow.next;
        }


        //开始翻转
        ListNode cur = slow.next;
        while(cur != null){
            ListNode curNext = cur.next;//记录下一个节点
            cur.next = slow;
            slow = cur;  //slow走到了最后
            cur = curNext;
        }

        //判断是否为回文
        while(head != slow){
            if(head.val != slow.val){
                return false;
            }

            if(head.next == slow){
                return true;
            }//偶数情况！
            head = head.next;
            slow = slow.next;
        }
        return true;
    }
}


