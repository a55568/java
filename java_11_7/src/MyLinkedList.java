public class MyLinkedList {
    public class ListNode{
        private int val;
        private ListNode prev;
        private ListNode next;

        public ListNode(int val){
            this.val = val;
        }
    }

    public ListNode head;//双向链表的头节点
    public ListNode last;//双向链表的尾巴


    public void create() {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(1);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(1  );


        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        this.head = node1;

        node4.prev = node3;
        node3.prev = node2;
        node2.prev = node1;
        this.last = node4;

    }

    //打印双向链表
    public void display(){
        ListNode cur = head;
        while(cur != null){
            System.out.println(cur.val+" ");
            cur = cur.next;
        }
    }

    //得到链表的长度
    public  int size(){
        ListNode cur = head;
        int count = 0;

        while (cur != null){
            count++;
            cur = cur.next;
        }

        return count;
    }

    //查找是否又关键字key在链表中
    public boolean contains(int key){
        ListNode cur = head;
        while (cur != null){
            if(cur.val == key){
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    //头插法
    public void addFirst(int val){
        ListNode node = new ListNode(val);
        if(head == null){
            node = head;
            node = last;
        }else {
         node.next = head;
         head.prev = node;
         head = node;
        }
    }

    //尾插法
    public void addLast(int date){
        ListNode node = new ListNode(date);
        if(last == null){
            node = head;
            node = last;
        }else {
            node.prev = last;
            last.next = node;

        }
    }

    //任意位置插入
    public void addIndex(int index,int date){
        checkIndex(index);

        if(index == 0){
            addFirst(date);
            return;
        }

        if(index == size()){
            addLast(date);
            return;
        }


        ListNode node = new ListNode(date);
        ListNode cur = head;
        if(head == null) {
            node = head;
            node = last;
        }

        for (int i = 0; i < index; i++) {
            cur = cur.next;
        }
        //注意顺应该先处理node的前一个的节点！！
        node.next = cur;
        cur.prev.next = node;
        node.prev = cur.prev;
        cur.prev = node;

    }

    private void checkIndex(int index) throws IndexOutOfExecption {
        if(index < 0||index > size()){
            throw new IndexOutOfExecption("Index不合法 " + index);
        }
    }

    //删除关键字key的节点
    public void remove(int key){
        ListNode cur = head;
        while (cur != null){
            if(cur.val == key){
                //删除头节点
                if(cur == head) {
                    head = head.next;
                    //考虑只有一个节点的情况
                    if (head == null) {
                        last = null;
                    } else {
                        head.prev = null;
                    }
                }else {
                    //删除中间节点
                    if(cur.next != null) {
                        cur.next.prev = cur.prev;
                        cur.prev.next = cur.next;
                    }else{
                    //删除尾巴节点
                        cur.prev.next = cur.next;
                        last = last.prev;
                    }
                }
                return;//删除后跳出循环
            }
            else {
                cur = cur.next;
            }
        }
    }


    //删除所有关键字key的节点
    public void allRemove(int key){
        ListNode cur = head;
        while (cur != null) {
            if (cur.val == key) {
                //删除头节点
                if (cur == head) {
                    head = head.next;
                    //考虑只有一个节点的情况
                    if (head == null) {
                        last = null;
                    } else {
                        head.prev = null;
                    }
                } else {
                    //删除中间节点
                    if (cur.next != null) {
                        cur.next.prev = cur.prev;
                        cur.prev.next = cur.next;
                    } else {
                        //删除尾巴节点
                        cur.prev.next = cur.next;
                        last = last.prev;
                    }
                }
                cur = cur.next;
            } else {
                cur = cur.next;
            }
        }
    }

    //清空双向链表
    public void clear(){
        ListNode cur = head;
        while (cur != null){
            ListNode curNext = cur.next;
           cur.next = null;
           cur.prev = null;
           cur = curNext;
        }
        head = null;
        last = null;
    }
}
