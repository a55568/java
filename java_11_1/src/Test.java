public class Test {
    public static int removeDuplicates(int[] nums) {
        int first = 1;
        int second = 1;
        while(first < nums.length){//利用双指针的思想与删除重复数据相似
            if(nums[first] != nums[first - 1]){
                nums[second] = nums[first];
                second++;
            }
            first++;
        }
        return second;

    }

    public static void main(String[] args) {
        int[] nums = {1,1,1,1,2};
        System.out.println(removeDuplicates(nums));
    }
}
