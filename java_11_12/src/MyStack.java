class MyStack {
    public Queue<Integer> qu1;
    public Queue<Integer> qu2;

    public MyStack() {
        qu1 = new LinkedList<>();
        qu2 = new LinkedList<>();
    }

    public void push(int x) {
        if(!qu1.isEmpty()){
            qu1.offer(x);
        }else if(!qu2.isEmpty()){
            qu2.offer(x);
        }else{
            qu1.offer(x);
        }
    }

    public int pop() {//关键是哪个不为空就放哪个队列
        if(empty()) {
            return -1;//此处说明栈为空(俩个队列都为空)
        }

        if(!qu1.isEmpty()) {
            int size = qu1.size();//如果不用一个值存储的话，qu1.size（）就会改变
            for(int i = 0;i < size - 1; i++){
                int tmp = qu1.poll();
                qu2.offer(tmp);
            }
            return qu1.poll();
        }else {
            int size = qu2.size();
            for(int i = 0;i < size - 1; i++){
                int tmp = qu2.poll();
                qu2.offer(tmp);
            }

            return qu2.poll();
        }
    }

    public int top() {
        int tmp = 0;
        if(empty()) {
            return -1;
        }

        if(!qu1.isEmpty()){
            int size = qu1.size();
            for(int i = 0; i< size; i++){
                tmp = qu1.poll();
                qu2.offer(tmp);
            }
            return tmp;
        }else {
            int size = qu2.size();
            for(int i = 0; i< size; i++){
                tmp = qu2.poll();
                qu1.offer(tmp);
            }
            return tmp;
        }

    }

    public boolean empty() {
        return qu1.isEmpty() && qu2.isEmpty();
    }
}
