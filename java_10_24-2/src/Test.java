
public class Test {

        public static int countSegments(String s) {
            int flag = 0;

            for (int i = 0; i < s.length(); i++) {
                if ((i == 0 || s.charAt(i - 1) == ' ') && s.charAt(i) != ' ') {
                    flag++;
                }
            }

            return flag;
        }

    public static void main(String[] args) {

        int a = countSegments("    ");
        System.out.println(a);
    }
}
//该下标对应的字符不为空格；
//该下标为初始下标（也解决了字符串为空的情况）或者该下标的前下标对应的字符为空格


