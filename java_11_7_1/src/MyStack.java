import java.util.Arrays;

public class MyStack {
    private int[] elem;
    private int usedSize;
    private static final int DEFAULT_CAPACITY = 10;

    public MyStack(){
        this.elem = new int[DEFAULT_CAPACITY];
    }

    //压栈
    public void push(int date){

        if(idFull()){
            this.elem = Arrays.copyOf(elem,2*elem.length);
        }
        this.elem[usedSize] = date;
        usedSize++;
    }

    public boolean idFull(){
        return usedSize == this.elem.length;
    }
//====================================================================//

    //出栈

    public int pop(){
        if(isEmpty()){
            throw new EmptyException("栈为空");
        }
        int x = this.elem[usedSize -1 ];
        this.usedSize--;
        return x;
    }
    public boolean isEmpty(){
        return this.usedSize == 0;
    }

//=========================================================================//


    //看栈顶的数据
    public int peek(){
        if(isEmpty()){
            throw new EmptyException("栈为空");
        }
        int x = this.elem[usedSize -1 ];
        return x;
    }
}
