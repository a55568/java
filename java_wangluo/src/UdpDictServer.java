import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;

public class UdpDictServer extends UdpEchoSever{

    private HashMap<String,String> hashMap = new HashMap<>();
    public UdpDictServer(int port) throws SocketException {
        super(port);

        hashMap.put("cat","小猫");
        hashMap.put("dog","小狗");
        hashMap.put("chicken","坤坤");

    }

    //start方法完全从父类这里继承下来即可
    //process方法进行重写，加入自己的业务逻辑，进行翻译

    @Override
    public String process(String request) {
        //参数是一个英文单词
        //返回值是一个对应的汉语

        return hashMap.getOrDefault(request,"您查的单词不存在");
    }

    public static void main(String[] args) throws IOException {
        UdpDictServer udpDictServer = new UdpDictServer(9090);
        udpDictServer.start();
    }
}
