import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UdpEchoSever {
    private DatagramSocket socket = null;

    public UdpEchoSever(int port) throws SocketException {
        socket = new DatagramSocket(port);
    }

    //服务器的启动逻辑

    public void start() throws IOException {
        System.out.println("服务器启动！");
        while (true){
            //每次循环，就是处理一个请求——响应过程
            //1，读取请求并解析
            DatagramPacket requstPacket = new DatagramPacket(new byte[4096],4096);
            socket.receive(requstPacket);

            //读取到的字节数组，转成String方便后面的逻辑处理
            String request = new String(requstPacket.getData(),0,requstPacket.getLength());//那这个字节数组的有限长度
            //2.根据请求计算响应（对于回显服务器，这一步什么都不用做）
            String response = process(request);
            //3.把响应返回到客户端
            //先得构造一个DatagramPacket 作为响应对象

            DatagramPacket responsePacket = new DatagramPacket(response.getBytes(),response.getBytes().length,
                    requstPacket.getSocketAddress());//此时传的是有内容的字节数组    getSocketAddress得到ip地址
            socket.send(responsePacket);

            //打印日志
            System.out.printf("[%s:%d] req: %s,resp: %s\n",requstPacket.getAddress().toString(),
                    requstPacket.getPort(),request,response) ;
        }
    }

    public String process(String request){
        return request;
    }

    public static void main(String[] args) throws IOException {
        UdpEchoSever sever = new UdpEchoSever(9090);//随意指定端口
        sever.start();
    }
}
