import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class UdpEchoChilient {
    private String serverIp;

    private int serverPort;

    private DatagramSocket socket = null;
    public UdpEchoChilient(String serverIp, int serverPort) throws SocketException {
        this.serverIp = serverIp;
        this.serverPort = serverPort;
        socket = new DatagramSocket();
    }

    public void start() throws IOException {
        System.out.println("客户端启动 ");
        Scanner scanner = new Scanner(System.in);
        while (true){
            //要做4个事情
            System.out.print ("->");//提示用户接下来输入内容
            //1.从控制台读取要发送的请求数据
            if(!scanner.hasNext()){
                break;
            }
            String request = scanner.next();
            //2.构造请求发送(有多种构造)值多看几遍
            DatagramPacket requestPacket = new DatagramPacket(request.getBytes(),request.getBytes().length,
                    InetAddress.getByName(serverIp),serverPort);
            socket.send(requestPacket);
            //3.读取服务器响应
            DatagramPacket responsePacket = new DatagramPacket( new byte[4096],4096);
            socket.receive(responsePacket);
            //4.把响应显示到控制台上
            String response = new String(responsePacket.getData(),0,responsePacket.getLength());
            System.out.println(response);
        }
    }

    public static void main(String[] args) throws IOException {
        UdpEchoChilient udpEchoChilient = new UdpEchoChilient("127.0.0.1",9090);
        udpEchoChilient.start();
    }
}
