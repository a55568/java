package tcp1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

 public class TcpEchoChilient {
    private Socket socket = null;

    public TcpEchoChilient(String severIp, int severPort) throws IOException {
        //此处可以把ip 和 port 直接给socket对象
        //由于tcp是有连接的 ，因此socket会保存好这俩个信息
        //因此 此处的TcpEchoChilient 类就不必保存了

        socket = new Socket(severIp, severPort);
    }

    public void start() {
        System.out.println("客户端启动！");
        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {

            Scanner scannerConsole = new Scanner(System.in);
            Scanner scannerNetwork = new Scanner(inputStream);
            PrintWriter writer = new PrintWriter(outputStream);
            while (true) {

                System.out.println("-> ");
                //这里的流程和udp客户端类似
                //1.从控制台读取输入的字符串
                if (!scannerConsole.hasNext()) {
                    break;
                }

                String requset = scannerConsole.next();

                //2.把请求发送给服务器，这里需要用println来发送，为了让发送的请求末尾带有\n
                //  这里和服务器的  scanner.next相呼应
                writer.println(requset);//（缓冲区对问题）
                //通过这个flush 主动刷新缓冲区，确保数据真的发出去了
                writer.flush();

                //3.从服务器读取响应
                String response = scannerNetwork.next();

                //4.把响应显示出来
                System.out.println(response);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws IOException {
        TcpEchoChilient tcpEchoChilient = new TcpEchoChilient("127.0.0.1",9090);
        tcpEchoChilient.start();
    }
}
