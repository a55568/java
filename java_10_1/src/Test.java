public class Test {
    public static void transform(int[] arr2 ){
       for(int x:arr2){
           System.out.println(x*2);
       }
    }
    public static void main(String[] args) {
        int[] arr2 = new int []{1,2,3,4,5};
        transform(arr2);

    }//实现一个方法 transform, 以数组为参数, 循环将数组中的每个元素 乘以 2 , 并设置到对应的数组元素上.
    //例如 原数组为 {1, 2, 3}, 修改之后为 {2, 4, 6}
}
