import java.util.Scanner;


public class Test {

        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNextInt()) {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                int z = scanner.nextInt();
                Sub sub = new Sub(x, y, z);
                System.out.println(sub.calculate(x,y,z));
            }
        }

    }


class Base {

    private int x;
    private int y;

    public Base(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;

    }
}


class Sub extends Base {

    private int z;
    public int calculate(int x, int y, int z){


        return x*y*z;


    }

    public Sub(int x, int y, int z) {
        super(x,y);
        this.z = z;
        //write your code here
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
