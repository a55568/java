
class Teacher{
    public String name;
    public void dance(){
        System.out.println(name+"在跳舞");
    }
}
class Student1 extends Teacher{
    public String name = "lisi";
    public void dance() {
        System.out.println(this.name +"学生在跳舞");
    }
}

class Student2 extends Teacher{
    public String name = "wangwu";
    public void dance() {
        System.out.println(this.name +"学生在跳舞");
    }
}
/* class  TestDemo{
     public static void print(String name){
         System.out.println(name + "在跳舞");
     }
}*/



public class Test {
    public static void main(String[] args) {
        Teacher student1 = new Student1();
        Teacher student2 = new Student2();
        func1(student1);
        func1(student2);//发生了向上转型
    }


    public static void func1(Teacher teacher) {
       teacher.dance();
    }
}
