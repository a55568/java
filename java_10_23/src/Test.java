import java.util.Arrays;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        char arr1[] = str.toCharArray();
        int arr2[] = new int[26];//计数数组

        for (int i = 0; i < arr1.length; i++) {

            arr2[arr1[i] - 'a'] ++;
        }
        /*System.out.println(Arrays.toString(arr2));*/

        for (int i = 0; i < arr1.length; i++) {
            if(arr2[arr1[i] - 'a'] == 1){
                System.out.println(i);
                break;
            }
        }

        //1.遍历原来的数组
        //2.把对应的字符放到对应的计数数组下标的位置（用x-a的方式节省计数数组的空间）
        //3.再次遍历原来的数组，看每个字符此时在计数数组中出现的次数


    }
}
