package demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CardList {
    private static final String[] SUITS= {"♥","♦","♣","♠"};//花色的数组


    public static List<Card> buyCards(){
        List<Card> list = new ArrayList<>();
        for (int i = 0; i < SUITS.length; i++) {
            for (int j = 0; j < 13; j++) {

                Card card = new Card(SUITS[i],j);
                list.add(card);
            }
        }
        return list;
    }

   //-----------洗牌-------------
    public static void shuffle(List<Card> list){
        Random random = new Random();

        for (int i =list.size()-1 ; i >0 ; i--) {
            int index = random.nextInt(i);
            swap(list,i,index);
        }

    }
    //---------交换----------------
    public static void swap(List<Card> list,int i,int j){
        Card temp = list.get(i);
        list.set(i,list.get(j));
        list.set(j,temp);
    }

    public static void main(String[] args) {
         List<Card> list = buyCards();
        System.out.println(list);
        shuffle(list);
        System.out.println(list);

      //用二维数组来实现每个人拿牌
        List<List<Card>> hand= new ArrayList<>();
        List<Card> hand1 = new ArrayList<>();//第一个人拿到的牌
        List<Card> hand2 = new ArrayList<>();
        List<Card> hand3 = new ArrayList<>();
        hand.add(hand1);
        hand.add(hand2);
        hand.add(hand3);



        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                Card card = list.remove(j);
                hand.get(j).add(card);
            }

        }

        System.out.println("第一个人的牌"+hand.get(0));
        System.out.println("第一个人的牌"+hand.get(1));
        System.out.println("第一个人的牌"+hand.get(2));

    }

}
