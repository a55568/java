package Book;

public class BookList {
    private Book[] books = new Book[10];//Book类的数组
    private int usedSize;//计数器 来记录 当前实际放的书！(也起到一个数组下标的作用)

    public Book getbook(int pos){
        return books[pos];
    }

    public void setBooks(int pos,Book book){
        books[pos] = book;
    }//这个方法用于放书

    public BookList(){
        this.books[0] = new Book("三国演义","罗贯中",12,"小说");
        this.books[1] = new Book("水浒传","施耐庵",12,"小说");
        this.books[2] = new Book("西游记","吴承恩",12,"小说");
        this.usedSize = 3;
    }//构造方法 初始化成员


    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }
}
