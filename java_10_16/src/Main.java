import Book.Book;
import Book.BookList;
import User.AdminUser;
import User.NormalUser;
import User.User;

import java.util.Scanner;

public class Main {

    //向上转型 达到一致性
    public static User login(){
        System.out.println("请输入的姓名:");
        Scanner scanner = new Scanner(System.in);
        String name =  scanner.nextLine();
        System.out.println("请输入你的身份：1--》管理员 2--》普通用户");
        int choice = scanner.nextInt();
        if(choice == 1){
           return new AdminUser(name);//怎么接受实例化的象--用return返回 再用向上转型
        } else  {
           return new NormalUser(name);
        }
    }


    public static void main(String[] args) {

        BookList bookList = new BookList();

        User user = login();
        while (true) {
            int choice = user.menu();//接受用户的功能
            user.doOperation(choice, bookList);
            //怎么和操作建立联系?
            //1.想让双方存自己对应的操作
            //2.调用方法
        }
    }
}
