package operation;

import Book.Book;
import Book.BookList;

import java.util.Scanner;

public class AddOperation implements IOperation{
    public void work(BookList bookList){
        System.out.println(" 增加图书！");

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入书名：");
        String name = scanner.nextLine();
        System.out.println("请输入作者：");
        String author = scanner.nextLine();
        System.out.println("请输入价格：");
        int price = scanner.nextInt();
        scanner.nextLine();
        System.out.println("请输入类型：");
        String type = scanner.nextLine();

        Book book = new Book(name,author,price,type);//实例化这本书

        int currentSize = bookList.getUsedSize();

        //如果有这本书，就不能添加这本书了
        for (int i = 0; i < currentSize; i++) {
            Book book1 = bookList.getbook(i);
            if(book1.getName().equals(name)){
                System.out.println("已经有这本书了，不能添加");
                return;
            }
        }

        //默认是放到了数组的最后的位置
        bookList.setBooks(currentSize,book);

        bookList.setUsedSize(currentSize+1);//放完一本书后，让下标加1





    }
}
