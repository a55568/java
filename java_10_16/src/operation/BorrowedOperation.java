package operation;

import Book.Book;
import Book.BookList;

import java.util.Scanner;

public class BorrowedOperation implements IOperation{
    public void work(BookList bookList) {
        System.out.println(" 借阅图书！");

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入书名：");
        String name = scanner.nextLine();

        int currentSize = bookList.getUsedSize();

        for (int i = 0; i < currentSize; i++) {
            Book book = bookList.getbook(i);

            if(book.getName().equals(name)){

                if(book.isBorrowed() == true){
                    System.out.println("这本书已被借出！");
                }else {
                    book.setBorrowed(true);
                }
                return;
            }
        }

        System.out.println("没有你要借阅的书！！");

    }
}
