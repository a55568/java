package operation;

import Book.Book;
import Book.BookList;

import java.util.Scanner;

public class FindOperation implements IOperation{
    public void work(BookList bookList) {
        System.out.println(" 查询图书！");

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要找的书：");
        String name = scanner.nextLine();

        int currentSize = bookList.getUsedSize();

        for (int i = 0; i < currentSize; i++) {
            Book book =  bookList.getbook(i);
            if(book.getName().equals(name)){
                System.out.println("找到了");
                System.out.println(book);
                return;
            }
        }

        System.out.println("不好意思，你没有这本书！");
    }
}
