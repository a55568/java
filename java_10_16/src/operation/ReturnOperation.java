package operation;

import Book.Book;
import Book.BookList;

import java.util.Scanner;

public class ReturnOperation implements IOperation{
    public void work(BookList bookList) {
        System.out.println(" 查询图书！");

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入书名：");
        String name = scanner.nextLine();

        int currentSize = bookList.getUsedSize();

        for (int i = 0; i < currentSize; i++) {
            Book book = bookList.getbook(i);

            if(book.getName().equals(name)){

                if(book.isBorrowed() == false){
                    System.out.println("这本书已被归还！");
                }else {
                    book.setBorrowed(false);
                }
                return;
            }
        }


        System.out.println("没有你要归还的书");
    }
}

