package operation;

import Book.Book;
import Book.BookList;

import java.util.Scanner;

public class DelOperation implements IOperation{
    public void work(BookList bookList){
        System.out.println(" 删除图书！");

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入书名：");
        String name = scanner.nextLine();

        int currentSize = bookList.getUsedSize();

        int flag = 1;

        for (int i = 0; i < currentSize; i++) {
            Book book1 = bookList.getbook(i);
            if(book1.getName().equals(name)){
                flag = i;
                break;
            }//找到这本书此时的下标
        }

        if(flag == 1){
            System.out.println("没有找到这本书！");
        }

        for (int i = flag; i < currentSize-1; i++) {
            Book book = bookList.getbook(i+1);
            bookList.setBooks(i,book);

        }

        bookList.setUsedSize(currentSize-1);//删除后数组末尾的下标-1

    }
}
