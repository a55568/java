package operation;

import Book.BookList;
import User.User;

public class ExitOperation implements IOperation {
    public void work(BookList bookList) {
        System.out.println(" 退出系统！");
        int currentSize = bookList.getUsedSize();
        for (int i = 0; i < currentSize; i++) {
            bookList.setBooks(i,null);//将每一本书置为空
        }
        bookList.setUsedSize(0);//存书量也置为0
        System.exit(0);

    }
}
