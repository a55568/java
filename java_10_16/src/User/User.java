package User;

import Book.BookList;
import operation.IOperation;

public abstract class User {
    protected String name;

    public IOperation[] iOperations;//定义数组
    public User(String name) {
        this.name = name;
    }
    public abstract int menu();//不用具体实现，因为动态绑定，所以可以变成抽象方法

    public void doOperation(int choice, BookList bookList){
        iOperations[choice].work( bookList);//调用方法
    }
}
