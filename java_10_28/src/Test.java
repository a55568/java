
class Alg<T extends Comparable <T>>{//泛型可以在数组存放任意类型
    Object[] arr = new Object[10];

    public void setArr(int pos,T val){
        arr[pos] = val;
    }

    public T getArr(int pos){
        return (T) arr[pos];

    }

    public T max(T []arr){//指定类型 比较此类型中最大的值

        T max= arr[0];
            for (int i = 0; i < arr.length ; i++) {
                if(arr[i].compareTo(max) > 0){
                    max = arr[i];
                }
            }
            return max;

    }

}

public class Test {
    public static void main(String[] args) {

        Alg<String> alg = new Alg<>();
        alg.setArr(0,"aaaa");
        alg.setArr(1,"bbbb");

        String[] arr ={"aaa","qqq"};
        System.out.println(alg.max(arr));
    }
}
