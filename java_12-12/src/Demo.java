import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Demo {
    public static void main(String[] args) throws SQLException {
        //1.先创建一个dataSource
        DataSource dataSource = new MysqlDataSource();//MysqlDataSource来自于导入的包
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java110? characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("123456");

        //2.建立连接
       Connection connection = dataSource.getConnection();

       //3.构造sql
        String sql = "select * from student";
        PreparedStatement statement = connection.prepareStatement(sql);

        //4.执行sql（和前面的不同了）
        ResultSet resultSet = statement.executeQuery();

        //5.遍历结果集合
        while (resultSet.next()){
            //针对这一行惊醒处理
            //取出列的数据
            int id = resultSet.getInt("id");
            String name = resultSet.getNString("name");
            System.out.println("id = " + id + ", name = " + name );
        }

        //6.释放资源
        resultSet.close();
        statement.close();
        connection.close();
    }

}
