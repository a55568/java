import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws SQLException {
        //1.先创建一个dataSource
        DataSource dataSource = new MysqlDataSource();//MysqlDataSource来自于导入的包
        ((MysqlDataSource)dataSource).setUrl("jdbc:mysql://127.0.0.1:3306/java110? characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("123456");

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入学号");
        int id = scanner.nextInt();
        System.out.println("请输入姓名");
        String name = scanner.next();

        //2.建立和数据库服务器之间的联系，连接好了之后进行后续的请求响应
        Connection connection = dataSource.getConnection();

        //3.构建sql
        String sql = "insert into student values(?,?)";

        //String sql = "delete from student where id = 1";
        //String sql = "update student set name = 'lisi' where id = 1";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1,id);
        statement.setString(2,name);
        //其他的操作只需要改这里面的内容

        //4.把sql发给服务器返回值是一个整数，表示影响到的行数
        int n = statement.executeUpdate();
        System.out.println("n= " + n);

        //5.关闭连接，释放资源
        statement.close();
        connection.close();
    }
}
