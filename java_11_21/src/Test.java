import java.util.PriorityQueue;


//最小k个数 用小根堆
public class Test {

    public int[] smallestK(int[] arr, int k) {

        int[] ret = new int[k];
        if(arr == null || k <= 0){
            return ret;
        }

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(arr.length);

        for(int i = 0; i<arr.length; i++){
            priorityQueue.offer(arr[i]);
        }

        for(int i = 0; i< k; i++){
            ret[i] = priorityQueue.poll();
        }

        return ret;
    }


//求数组中第k大的元素 （用小根堆）
    public int findKthLargest(int[] nums, int k) {
        int[] ret = new int[k];
        if(nums == null ){
            return 0;
        }

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(nums.length);

        for(int i = 0; i< k; i++){
            priorityQueue.offer(nums[i]);
        }


        for(int i = k; i< nums.length;i ++){
            int top = priorityQueue.peek();
            if(top < nums[i]){
                priorityQueue.poll();
                priorityQueue.offer(nums[i]);
            }
        }

        return priorityQueue.peek();
    }





}
