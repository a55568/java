 class TreeNode {
     int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }
public class Solution {//化问题化小问题 （左树或右树的长度的最大值+1）
    public int maxDepth(TreeNode root) {
        if(root == null){
            return 0;
        }

        int leftLength = maxDepth(root.left);
        int rightLength = maxDepth(root.right);

        return (leftLength > rightLength ? leftLength + 1 : rightLength + 1);
    }
}
