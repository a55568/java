import java.util.Arrays;

public class Test {
   /* public static double avg(int[] arr){
       double ret = 0;
       double sum ;
            for(int x:arr){
                ret += x;
        }
             sum = ret / arr.length;
            return sum;
    }



    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3,4};
        System.out.println(avg(arr));

    }//实现一个方法 avg, 以数组为参数, 求数组中所有元素的平均值(注意方法的返回值类型).*/


    public static void main(String[] args) {
        int[] arr = new int[]{1,5,6,2,8,9,5,20,11};
        for (int i = 0; i < arr.length; i++) {//控制趟数

                if(arr[i] % 2==0){
                    for (int j = i; j < arr.length; j++) {//如果有偶数 ，从偶数后面开始遍历
                        if(arr[j] % 2!= 0){//如果有奇数 和前面的偶数进行交换
                            int temp = arr[i];
                            arr[i] = arr[j];
                            arr[j] = temp;
                        }
                    }
                }
        }
        System.out.println(Arrays.toString(arr));
    }//调整数组顺序使得奇数位于偶数之前。调整之后，不关心大小顺序。
   // 如数组：[1,2,3,4,5,6] 调整后可能是：[1, 5, 3, 4, 2, 6]
}
