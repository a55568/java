import java.util.Arrays;

 class MyArraylist {

     public int[] elem;
     public int usedSize;//0
     //默认容量
     private static final int DEFAULT_SIZE = 10;

     public MyArraylist() {
         this.elem = new int[DEFAULT_SIZE];
     }

     /**
      * 打印顺序表:
      * 根据usedSize判断即可
      */
     public void display() {
         for (int i = 0; i < usedSize; i++) {
             System.out.print(this.elem[i] + " ");
         }
     }

     // 新增元素,默认在数组最后新增
     public void add(int data) {
         if (isFull()) {//判断是否扩容
             this.elem = Arrays.copyOf(this.elem, 2 * this.elem.length);
         }
         this.elem[usedSize] = data;
         this.usedSize++;
     }

     /**
      * 判断当前的顺序表是不是满的！
      *
      * @return true:满   false代表空
      */
     public boolean isFull() {
         if (this.usedSize == this.elem.length) {
             return true;
         } else {
             return false;
         }
     }

     private boolean checkPosInAdd(int pos)  {
         if (pos < 0 || pos > elem.length) {
             throw new PosOutOfException(pos+" 位置不合适");//可以自定义一个异常，抛出

         } else return true;
     }

     // 在 pos 位置新增元素
     public void add(int pos, int data) {
         if (isFull()) {//判断是否扩容
             this.elem = Arrays.copyOf(this.elem, 2 * this.elem.length);
         }

         if (checkPosInAdd(pos)) {
             for (int i = usedSize - 1; i >= pos; i--) {
                 this.elem[i + 1] = this.elem[i];
             }
             this.elem[pos] = data;
             this.usedSize++;

         }
     }

     // 判定是否包含某个元素
     public boolean contains(int toFind) {
         for (int i = 0; i < this.usedSize; i++) {
             if (this.elem[i] == toFind) {
                 return true;
             }
         }
         return false;
     }

     // 查找某个元素对应的位置
     public int indexOf(int toFind) {
         for (int i = 0; i < this.usedSize; i++) {
             if (this.elem[i] == toFind) {
                 return i;
             }
         }
         return -1;
     }
         // 获取 pos 位置的元素
    public int get(int pos) {
         if(checkPosInAdd(pos)){
             return this.elem[pos];
         }
         return -1;

    }


         // 给 pos 位置的元素设为【更新为】 value
    public void set(int pos, int value) {
        if(checkPosInAdd(pos)){
            this.elem[pos] = value;
        }
    }

         /**
          * 删除第一次出现的关键字key
          */
    public void remove(int key) {
        int ret = 0;
        for (int i = 0; i < this.usedSize; i++) {
            if (this.elem[i] == key) {
                ret = i;
            }
        }
        for (int i = ret; i < this.usedSize-1; i++) {
            this.elem[i] = this.elem[i+1];
        }
        this.usedSize--;
    }

 }


public class Test {
    public static void main(String[] args) {
        MyArraylist myArraylist = new MyArraylist();
        myArraylist.add(0);
        myArraylist.add(1);
        myArraylist.add(2);
        myArraylist.add(3);
        myArraylist.add(4);
        myArraylist.display();
        myArraylist.remove(2);
        myArraylist.display();
    }

}
