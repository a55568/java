public class PosOutOfException extends RuntimeException{
    public PosOutOfException() {
    }

    public PosOutOfException(String message) {
        super(message);
    }
}
