import java.util.Scanner;

public class Test {
    public static void move(char pose1,char pose2){
        System.out.println(pose1 + "->" + pose2);
    }
    public static void han(int n,char pose1,char pose2,char pose3){
        if(n==1){
            move(pose1,pose3);
        }else {
            han(n - 1, pose1, pose3, pose2);//将c当为中转 把n-1个盘子放到b上
            move(pose1, pose3);//将最后一个放在c上
            han(n - 1, pose2, pose1, pose3);//将a当为中转 将剩下的放在c上
        }
    }//思路是 目标地方，中转地方，起始地方，一直在变，每一次的目的是 将n-1个放在中转上，将最后一个放在目标上
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        han(n,'a','b','c');

    }//汉诺塔问题
}
